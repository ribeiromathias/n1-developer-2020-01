const proxIcon = '<img class="banner-angle-size banner-angle-right" src="src/svgs/angle-right-solid.svg" alt:"right">';
const antIcon = '<img class="banner-angle-size banner-angle-left" src="src/svgs/angle-left-solid.svg" alt:"left">';


$('.owl-one').owlCarousel({
   loop: true,
   nav: true,
   navText: [
       proxIcon,
       antIcon
   ],
   responsive: {
       0: {
           items: 1
       },
       1024: {
           items: 1
       }
   }
})

const nextIcon = '<img class="angle-size angle-right" src="src/svgs/angle-right-solid.svg" alt:"right">';
const prevIcon = '<img class="angle-size angle-left" src="src/svgs/angle-left-solid.svg" alt:"left">';



$('.owl-two').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
        nextIcon,
        prevIcon
    ],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        768: {
         items: 2
        },
        1024: {
            items: 4
        }
    }
 })